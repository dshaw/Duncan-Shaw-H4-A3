#include <string>
#include <iostream>
#include <stdlib.h>
using namespace std;

//==>SAM Overall the methodology is correct, but there are a few implementation curiosities


//==>SAM Ahhhhhhh, don't use global variables unless you really need to


int main ()
{
	int count;
	// the only way I could figure out how to get the number in the program.
	// at first I was going to put each digit into a string from here, but then I found a better way.
	string number= 		"7316717653133062491922511967442657474235534919493496983520312774506326239578318016984801869478851843858615607891129494954595017379583319528532088055111254069874715852386305071569329096329522744304355766896648950445244523161731856403098711121722383113622298934233803081353362766142828064444866452387493035890729629049156044077239071381051585930796086670172427121883998797908792274921901699720888093776657273330010533678812202354218097512545405947522435258490771167055601360483958644670632441572215539753697817977846174064955149290862569321978468622482839722413756570560574902614079729686524145351004748216637048440319989000889524345065854122758866688116427171479924442928230863465674813919123162824586178664583591245665294765456828489128831426076900422421902267105562632111110937054421750694165896040807198403850962455444362981230987879927244284909188845801561660979191338754992005240636899125607176060588611646710940507754100225698315520005593572972571636269561882670428252483600823257530420752963450";
	//It's really important to use high precision numbers to get the correct answer.
	double product;
	double largestproduct= 0;
	//go from the first 13 digits, to the last 13 digits
	for (int i=0; i< number.length() -12;i++)
	{
		//It's important to reinitialize product each time so we don't getg larger and larger numbers
		//the -(int) num-48 turns it from a char to an int
//==> SAM Sorry, I don't see how subtracting 48 from an integer that has been casted from a character is going to give you the proper integer.  Can you explain more?
//the ascii character for numbers is 48 higher that their integer value. so 0 isn't 0 as a charcter, but 48 (110000).
		product=((int) number[i]-48);
		//multiply that first digit by the next 12
		for (int n=i+1;n<i+13;n++)
		{
			product= product*((int) number[n]-48);
		}
		//if this is the largest product yet, store that number and the digit it starts on
		if (product>largestproduct)
		{
			count=i;
			largestproduct=product;
		}
	}
	//print the largest number
	cout<<"The largest product is: "<<fixed<<largestproduct<<endl;
	cout<<"The thirteen digits are: ";
	//print the 13 corresponding digits
	for (int j=count;j < count + 13;j++)
	{
		cout<<((int) number[j]-48);
	}
	cout<<endl;
}
