#include <vector>
#include <iostream>
#include <stdlib.h>
#include <math.h>
using namespace std;

//global variable
double pi=3.14159;

//==> SAM This is close to being correct, but still requires revision.  Also, when I compiled it, I got these two warnings.  They are telling you something valuable, and just because it compiled without errors, doesn't mean that everything is ok.
/*
meehan:graded > g++ Duncanshaw_2.20.2017_H3.2.cpp
Duncanshaw_2.20.2017_H3.2.cpp:57:3: warning: ignoring return value of function declared with const attribute [-Wunused-value]
                atan(pz/magp0);
                ^~~~ ~~~~~~~~
Duncanshaw_2.20.2017_H3.2.cpp:59:3: warning: ignoring return value of function declared with const attribute [-Wunused-value]
                atan(h/magp1);
                ^~~~ ~~~~~~~
2 warnings generated.

*/
//I'm not getting the errors now that I've done all the other fixes so probably Fixed

//==>SAM Where is the constructor of your class?
//Fixed

//this is the class for my four vector
class FourVector
{

	//public members
	public:
	double E;
	double px;
	double py;
	double pz;

	//this initializes the vector
	void SetFourVector(double a, double b, double c, double d)
	{
		E=a;
		px=b;
		py=c;
		pz=d;
	}

	//calculate the invariant mass
	double Mass ()
	{
		double temp=sqrt(px*px+py*py+pz*pz);
		temp=E*E+temp;
		return sqrt(temp);
	}

	//calculate eta
	double Eta ()
	{
	//==>SAM I don't think this is a definition of eta.  As you can see here - https://en.wikipedia.org/wiki/Pseudorapidity - you need to use the "hyperbolic" arc tangent, whereas you use the arctangent only.
	//Fixed
		double magp=sqrt(px*px+py*py+pz*pz);
		return atanh(pz/magp);
	}

	//calculate the difference in asmuthal angle between this and another four vector
	//==> SAM - These are not good choices for the arguments to a function since its makes it very hard to read
	//          The assignment also called for you to pass as an argument a four vector instance, not four floats
	//          The definitions of "phi" are also not correct.  You should draw a picture and you will see these are closer to
	//          The polar angle "theta" whereas phi is the azimuthal angle and so only needs to use px and py in its definition
	//          Finally, when taking the difference between to phi values, you have to be careful because in practice, the difference between two angles can never be greater than PI
	//Fixed
	double DeltaPhi (double E1, double px1, double py1, double pz1)
	{
		//calculate phi0 and phi1
		double phi0=atan2(py,px);
		double phi1=atan2(py1,px1);
		//find the absolute vlue of the difference
		double DPhi=sqrt((phi0-phi1)*(phi0-phi1));
		//if the difference is greater than pi, subtract the result from pi
		if (DPhi>pi) DPhi=pi-DPhi;
		return sqrt(DPhi*DPhi);
	}

	//calculate the diference in Eta between this and another four vector
//==>SAM By and large the same issues as with the previous function are present here.
//Fixed?
	double DeltaEta (double E, double px1, double py1, double pz1)
	{
		//cacluate the magnitude of the momentum for each system
		double magp0=sqrt(px*px+py*py+pz*pz);
		magp0=atan(pz/magp0);
		double magp1=sqrt(px1*px1+py1*py1+pz1*pz1);
		magp1=atan(pz1/magp1);
		//find the absolute value of the difference
		double DEta=sqrt((magp0-magp1)*(magp0-magp1));
		return DEta;
	}
};


int main ()
{
	cout<<"Particle 1:"<<endl<<endl;

	//input the values of the first four vector
	FourVector E1;
	double n, m, o, p;
	cout<<"Enter the particle energy"<<endl;
	cin>>n;
	cout<<"Enter the x momentum"<<endl;
	cin>>m;
	cout<<"Enter the y momentum"<<endl;
	cin>>o;
	cout<<"Enter the z momentum"<<endl;
	cin>>p;

	//return the mass and psudorapidity
	E1.SetFourVector(n,m,o,p);
	cout<<"The invariant mass of the system is: "<<E1.Mass()<<endl;
	cout<<"The psuedorapidity of the system is: "<<E1.Eta()<<endl;

	cout<<endl<<"Particle 2:"<<endl<<endl;

	//input the values of the second particle
	FourVector E2;
	cout<<"Enter the particle energy"<<endl;
	cin>>n;
	cout<<"Enter the x momentum"<<endl;
	cin>>m;
	cout<<"Enter the y momentum"<<endl;
	cin>>o;
	cout<<"Enter the z momentum"<<endl;
	cin>>p;
	E2.SetFourVector(n,m,o,p);

	//calculate the differences of eta and phi
	cout<<"The Delta Phi of the two particles is: ";
	cout<<E1.DeltaPhi(E2.E,E2.px,E2.py,E2.pz)<<endl;
	cout<<"The Delta Eta of the two particles is: ";
	cout<<E1.DeltaEta(E2.E,E2.px,E2.py,E2.pz)<<endl;

}
