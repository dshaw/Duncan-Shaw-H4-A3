
#include <iostream>
#include <vector>
#include <stdlib.h>
using namespace std;

//==>SAM This is good but the one thing that is not following the directions is what you put into the array.  You shoudld be entering a random number
// that is distributed from [0,1], whereas you are either entering 0 or 1.
//Fixed

int input;
int main ()
{
	//initialize my vector
	std:vector<float> fvect;

	//perform this loop until you terminate it from within
  	while (0==0)
    	{

		//choose whether to add another value or stop
      		cout<<"Enter 1 to continue or 0 to terminate"<<endl;
		cin>>input;
		cout<<"-------------------"<<endl;

		//if you choose to stop end the program
		if (input==0)
		{
			break;
		}

		//if you choose to continue, add either 0 or 1 and then print the array
		if (input==1)
		{
			double n= (double)rand() / ((double)RAND_MAX);
			fvect.push_back(n);
			for (int c = 0 ; c < fvect.size(); c++ )
			{
			std::cout<<fvect[c]<<"\n";
			}
		}

		//if anything else is entered end the code
		else
		{
		cout<<"I told you to enter either 0, or 1."<<endl<<"It's a simple request, really."<<endl;
		break;
		}
	}
	cout<<"Enter any key to end program"<<endl;
	cin>>input;
}
